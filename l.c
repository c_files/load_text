/*
  from  gui main.c
  https://gitorious.org/maximus/book/source/47ce485c4266b3d5fc62f685f001ce7606ce426f:
  by 
  http://mathr.co.uk/blog/   


------------------ licence---------------------
Copyright © 2012,2013,2014,2015 Claude Heiland-Allen

License GPL3+ http://www.gnu.org/licenses/gpl.html
-----------------------------
  loads txt file with parameters
  http://qualapps.blogspot.com/2009/01/unicode-bom-handling-in-c-run-time.html
  http://www.joelonsoftware.com/articles/Unicode.html
  http://programmers.stackexchange.com/questions/102205/should-utf-16-be-considered-harmful
  http://utf8everywhere.org/
  http://www.codeguru.com/cpp/misc/misc/multi-lingualsupport/article.php/c10451/The-Basics-of-UTF8.htm
  http://stackoverflow.com/questions/2113270/how-to-read-unicode-utf-8-binary-file-line-by-line
  http://www.cl.cam.ac.uk/~mgk25/unicode.html


sscanf :
 http://linux.die.net/man/3/sscanf
 http://crasseux.com/books/ctutorial/sscanf.html

------------------------------doc---------------------

How to compile : 

  gcc l.c -lmpfr -lgmp -Wall

How to run :

./a.out BOM.txt

output :

file BOM.txt opened
 removed non-printing char = � ( non -ASCII char ) = -17  (signed 8 bit) = 239 (unsigned 8 bit) =  0xEF (hexadecimal) 
 removed non-printing char = � ( non -ASCII char ) = -69  (signed 8 bit) = 187 (unsigned 8 bit) =  0xBB (hexadecimal) 
 removed non-printing char = � ( non -ASCII char ) = -65  (signed 8 bit) = 191 (unsigned 8 bit) =  0xBF (hexadecimal) 
size line found :size of image in pixels : width = 2000   ; height = 1000 
view line found : 
precision p  =  154
center =  4.026223242141852148037824724584025835755135011397558229834670059078770738098405825309964941559830828549814522323256615887743237180984579026699066162109375e-1 ; 3.414661267010330639258781735300664742792277354173579465734033546246283957809062043866256934508704364086250923149366137687721334259549621492624282836914062e-1
radius =  5.780961321010291129746524187279121451180788424015420729906659988184540842229546210251049892406354472079271846156824481533441671857929584398611913850648352e-2077
line of comment found : // http://www.fractalforums.com/images-showcase-(rate-my-fractal)/baby-monster/
file BOM.txt closed



./a.out filament.txt

output :

file filament.txt opened
view line found : 
precision p  =  54
view line not valid, check chars 
file filament.txt closed


because of invalid char : 



*/
#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strncmp
#include <ctype.h> // isascii
#include <mpfr.h>



struct view {
  mpfr_t cx, cy, radius, pixel_spacing;
};

static struct view *view_new() {
  struct view *v = malloc(sizeof(struct view));
  mpfr_init2(v->cx, 53);
  mpfr_init2(v->cy, 53);
  mpfr_init2(v->radius, 53);
  mpfr_init2(v->pixel_spacing, 53);
  return v;
}

static void view_delete(struct view *v) {
  mpfr_clear(v->cx);
  mpfr_clear(v->cy);
  mpfr_clear(v->radius);
  mpfr_clear(v->pixel_spacing);
  free(v);
}


//http://stackoverflow.com/questions/5457608/how-to-remove-a-character-from-a-string-in-c?rq=1
// Fabio Cabral
void removeChar(char *str, char garbage) {

    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != garbage) dst++;
    }
    *dst = '\0';
}





// https://en.wikipedia.org/wiki/Serialization
// read data from file 
static int deserialize(const char *filename) {

  // open file in a text mode 
  FILE *in = fopen(filename, "r");

  if (in) {
    printf( "file %s opened\n", filename);
    char *line = 0;
    size_t linelen = 0;
    ssize_t readlen = 0; // bytes_read = length of the line 
    
    // read line 
    while (-1 != (readlen = getline(&line, &linelen, in)))     
      {
        // 
	if (readlen && line[readlen-1] == '\n') { 
	  line[readlen - 1] = 0; }

	// remove BOM 
        while  (! isascii(line[0])) 
               {printf(" removed non-printing char = %c ( non -ASCII char ) = %3d  (signed 8 bit) = %u (unsigned 8 bit) =  0x%.2X (hexadecimal) \n", line[0], line[0], (unsigned char) line[0], (unsigned char) line[0]);
                removeChar(line,line[0]);
                }
                
       
	// size 
	if (0 == strncmp(line, "size ", 5)) 
	  {

	    printf( "size line found :");
    
	    int w = 0, h = 0;
	    sscanf(line + 5, "%d %d\n", &w, &h);
	    printf( "size of image in pixels : width = %d   ; height = %d \n", w,h);
	    // resize_image(w, h); // FIXME TODO make this work...
	  } 
         
        //  view
        else if (0 == strncmp(line, "view ", 5)) 
	  {
	    printf( "view line found : \n");
            int n=4;  // number of values which should be inside view line       
	    int p = 53;
            int set_result;
            int sscanf_result;
	    char *xs = malloc(readlen);   
	    char *ys = malloc(readlen);
	    char *rs = malloc(readlen);
            sscanf_result = sscanf(line + 5, "%d %s %s %s", &p, xs, ys, rs);
            if ( sscanf_result < 4) { // error 
                    printf( "could not extract %d values from view line, only %d  \n", n, sscanf_result ); 
                    free(line);
                    fclose(in);
                    printf( "file %s closed\n", filename);
                    // free memory
                    free(xs);
	            free(ys);
	            free(rs);
	            return 1 ;}
			

	    struct view *v = view_new();
	    mpfr_set_prec(v->cx, p);
	    mpfr_set_prec(v->cy, p);
            printf ("precision p  =  %d\n", p);
	       

            //   mpfr_set_str function returns 0 if the entire string up to the final null character is a valid number in base base; otherwise it returns −1
	    set_result = mpfr_set_str(v->cx, xs, 0, GMP_RNDN);
	    set_result = set_result + mpfr_set_str(v->cy, ys, 0, GMP_RNDN);
	    set_result = set_result + mpfr_set_str(v->radius, rs, 0, GMP_RNDN);
	    // error 
            if ( set_result < 0) { // error 
                    printf( "view line not valid, check chars \n"); 
                    free(line);
                    fclose(in);
                    printf( "file %s closed\n", filename);
                    // free memory  because return without free is causing a memory leak
                    free(xs);
	            free(ys);
	            free(rs);
	            view_delete(v);
                    return 1 ;}

	    printf ("center =  "); 
	    mpfr_out_str (stdout, 10, p, v->cx, MPFR_RNDD);
	    printf (" ; ");
	    mpfr_out_str (stdout, 10, p, v->cy, MPFR_RNDD);
	    putchar ('\n');  

	    printf ("radius =  ");
	    mpfr_out_str (stdout, 10, p, v->radius, MPFR_RNDD);
	    putchar ('\n');  


	    free(xs);
	    free(ys);
	    free(rs);
	    view_delete(v);
        
	  } 
         
	else if (0 == strncmp(line, "ray_out ", 8)) 
	  {
	    printf( "ray_out line found :\n");
	    int p = 53;
	    char *xs = malloc(readlen);
	    char *ys = malloc(readlen);
	    sscanf(line + 8, "%d %s %s", &p, xs, ys);
	    mpfr_t cx, cy;
	    mpfr_init2(cx, p);
	    mpfr_init2(cy, p);
	    mpfr_set_str(cx, xs, 0, GMP_RNDN);
	    mpfr_set_str(cy, ys, 0, GMP_RNDN);
	    free(xs);
	    free(ys);
           
	    mpfr_clear(cx);
	    mpfr_clear(cy);
	  } 
        
        else if (0 == strncmp(line, "ray_in ", 7)) {
	  printf( "ray_in line found : \n");
	  int depth = 1000;
	  char *as = malloc(readlen);
	  sscanf(line + 7, "%d %s", &depth, as);
        
	  free(as);
	} 
      
	else if (0 == strncmp(line, "text ", 20)) {
	  printf( "text line found : \n");
	  //int p = 53;
	  char *xs= malloc(readlen);
	  char *ys = malloc(readlen);
	  char *ss = malloc(readlen);
	  sscanf(line + 20, "%s %s %s \n", xs, ys, ss);
        
       
	  free(xs);
	  free(ys);
	  free(ss);
	}

      
	else if (0 == strncmp(line, "//", 2 )) {
	  printf( "line of comment found : %s\n", line );
        
        }
	else printf( "not-known line found ! Maybe line with BOM ? \n");


      } // while 

    free(line);
    fclose(in);
    printf( "file %s closed\n", filename);
    return 0;} 
 
  // if (in)
  else {
    printf( "file %s not opened = error !\n", filename);  
    return 1;}
}








int main (int argc, const char* argv[]) 
{

  if (argc != 2) {
    fprintf(stderr, "program opens text file \n");
    fprintf(stderr, "program input : text file with parameters \n");
    fprintf(stderr, "usage: %s filename\n", argv[0]);
    return 1;
  }

  deserialize(argv[1]);


  return 0;
}
